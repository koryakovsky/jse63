package ru.nlmk.study.jse63.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.nlmk.study.jse63.config.HibernateConfig;

import javax.persistence.LockModeType;
import java.io.Serializable;

public class CommonRepository {

    private SessionFactory sessionFactory = HibernateConfig.getSessionFactory();

    public <T> void save(T entity) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        session.save(entity);

        tx.commit();
        session.close();
    }

    public <T> T getById(Class<T> entityClass, Serializable id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        T entity = session.find(entityClass, id, LockModeType.PESSIMISTIC_WRITE);

        tx.commit();
        session.close();

        return entity;
    }
}
