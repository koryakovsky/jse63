package ru.nlmk.study.jse63.config;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import ru.nlmk.study.jse63.model.Book;
import ru.nlmk.study.jse63.model.Car;
import ru.nlmk.study.jse63.model.Customer;
import ru.nlmk.study.jse63.model.Passport;


public class HibernateConfig {

    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory != null) return sessionFactory;

        Configuration cfg = new Configuration();
        cfg.addAnnotatedClass(Customer.class);
        cfg.addAnnotatedClass(Passport.class);
        cfg.addAnnotatedClass(Car.class);
        cfg.addAnnotatedClass(Book.class);


        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(cfg.getProperties())
                .build();

        sessionFactory = cfg.buildSessionFactory(serviceRegistry);

        return sessionFactory;
    }
}
