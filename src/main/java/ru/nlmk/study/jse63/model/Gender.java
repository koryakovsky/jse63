package ru.nlmk.study.jse63.model;

public enum Gender {

    FEMALE,
    MALE
}
